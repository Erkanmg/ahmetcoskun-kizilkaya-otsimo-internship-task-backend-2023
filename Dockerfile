# Use the official golang image to create a build artifact.
# This is based on the official Debian-based image.
FROM golang:1.20 AS build-env

# Set the working directory outside $GOPATH to enable the support for modules.
WORKDIR /src

# Copy all the files
COPY . .

# Download all dependancies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Compile the binary
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /app cmd/main.go

# Use a multi-stage build to create a lean runtime image.
FROM alpine

# Set the working directory to /app
WORKDIR /app

# Copy the binary from the build-env
COPY --from=build-env /src/cmd/dataset.json .
COPY --from=build-env /app .
RUN chmod +x ./app
# Run the application
CMD ["./app"]