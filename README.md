# Transparent Restaurant Backend
In this task, you're going to implement a REST API for a interacting with a menu of a restaurant. The menu is given to you as a JSON file which you will parse and perform operations on. The required features will be listed below.
## Description
In this restaurant, honesty is extremely promoted. So extreme, that the restaurant declares that differing quality of ingredients are used in their meals. Like that's not enough, it also allows the customers to choose the ingredients of each meal in different qualities. Each ingredient has the following quality levels:



```
## Endpoints
PATH: /listMeals
METHOD: GET
PARAMS:
  is_vegetarian: (boolean, optional) default=false
  is_vegan: (boolean, optional) default=false
SAMPLE: http://34.165.200.59:1000/listMeals
```
![Listing Meals](https://i.hizliresim.com/dfsyicd.jpg)

### Getting an Item from Menu

This endpoint takes a meal ID and returns its name and ingredients with each option of ingredients included.

```
PATH: /getMeal
METHOD: GET
PARAMS:
    id: N (integer, required)
SAMPLE: http://34.165.200.59:1000/getMeal?id=2
```
![Get Meal](https://i.hizliresim.com/487r5gb.jpg)

### Quality Calculation With Ingredient Qualities

This endpoint takes a meal id with all of its ingredients' quality selecitons and returns the resulting quality score. If an ingredient's quality is not specified, "high" quality should be assumed by default.

```
PATH: /quality
METHOD: POST
PARAMS:
  meal_id: (integer, required)
  <ingredient-1>: (enum, values: ["high", "medium", "low"], optional) default="high"
  <ingredient-2>: (enum, values: ["high", "medium", "low"], optional) default="high"
  ...
  SAMPLE: http://34.165.200.59:1000/quality?id=2
```
![Quality](https://i.hizliresim.com/9qmrkui.jpg)

### Price Calculation With Ingredient Qualities

This endpoint takes a meal id with all of its ingredients' quality selecitons and returns the resulting price. If an ingredient's quality is not specified, "high" quality should be assumed by default.

```
PATH: /price
METHOD: POST
PARAMS:
  meal_id: (integer, required)
  <ingredient-1>: (enum, values: ["high", "medium", "low"], optional) default="high"
  <ingredient-2>: (enum, values: ["high", "medium", "low"], optional) default="high"
  ...
  SAMPLE: http://34.165.200.59:1000/price?id=2
```
![Price](https://i.hizliresim.com/48to0k1.jpg)
### I'm Feeling Lucky

This endpoint returns a randomly selected meal of random quality parameters, with an option to set a budget.

```
PATH: /random
METHOD: POST
PARAMS:
  budget: (double, optional) default=unlimited
  SAMPLE: http://34.165.200.59:1000/random
```
![Random](https://i.hizliresim.com/gb5u9gk.jpg)
### Search With Query
```
PATH: /search
METHOD: GET
PARAMS:
  query: (string, required)
  SAMPLE: http://34.165.200.59:1000/search?query=beef
```
![Search](https://i.hizliresim.com/nmj4p52.jpg)

### Finding the Highest Quality Meal For Given Budget

This endpoint takes a budget as input and yields the highest-quality meal that can be prepared for that budget and how much it costs.

```
PATH: /findHighest
METHOD: POST
PARAMS:
  budget: (double, required)
  is_vegetarian: (boolean, optional) default=false
  is_vegan: (boolean, optional) default=false
  SAMPLE: http://34.165.200.59:1000/findHighest?budget=35
```
![Search](https://i.hizliresim.com/88p8fd3.jpg)
### Finding the Highest Quality Version of a Meal For Given Budget

This endpoint takes a budget and meal id as input and yields the highest-quality version of it that can be prepared for that budget and how much it costs.

```
PATH: /findHighestOfMeal
METHOD: POST
PARAMS:
  meal_id: (integer, required)
  budget: (double, required)
  SAMPLE: http://34.165.200.59:1000/findHighestOfMeal?id=3
```
![Search](https://i.hizliresim.com/nmj4p52.jpg)
## Jenkins And Docker (CI/CD)
This project has been set up on a machine on the Google Cloud Platform using CentOS8 operating system, with Docker and Jenkins for CI/CD structure. Thanks to Jenkins, every push triggers automatic acquisition of a new image on Docker.
## Project Structure
```
├───cmd
└───pkg
    ├───api
    ├───handler
    ├───model
    ├───parse
    ├───repository
    ├───router
    └───service
  ```
