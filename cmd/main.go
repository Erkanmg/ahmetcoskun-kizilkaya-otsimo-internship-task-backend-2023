package main

import (
	"log"
	"net/http"

	"github.com/ahmetk3436/otsimo/pkg/api"
	"github.com/ahmetk3436/otsimo/pkg/handler"
	"github.com/ahmetk3436/otsimo/pkg/parse"
	"github.com/ahmetk3436/otsimo/pkg/repository"
	"github.com/ahmetk3436/otsimo/pkg/router"
	"github.com/ahmetk3436/otsimo/pkg/service"
)

func main() {
	meals, err := parse.ParseMeals()
	if err != nil {
		log.Fatalf("Failed to parse meals: %v", err)
	}
	ingredients, err := parse.ParseIngredients()
	if err != nil {
		log.Fatalf("Failed to parse ingredients: %v", err)
	}

	mealRepository := repository.NewRepository()
	mealService := service.NewMealService(mealRepository)
	mealAPI := api.NewPostAPI(mealService)
	mealHandler := handler.NewMealHandler(mealAPI, meals, ingredients)

	r := router.NewRouter(*mealHandler)
	log.Fatal(http.ListenAndServe(":1000", r))
}
