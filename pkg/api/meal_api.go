package api

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/ahmetk3436/otsimo/pkg/model"
	"github.com/ahmetk3436/otsimo/pkg/service"
)

// MealAPI ...
type MealAPI struct {
	MealService service.MealService
}

// NewPostAPI ...
func NewPostAPI(p service.MealService) MealAPI {
	return MealAPI{MealService: p}
}

func (p MealAPI) ListMeals(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	isVegan, err := strconv.ParseBool(r.URL.Query().Get("is_vegan"))
	if err != nil {
		isVegan = false
	}

	isVegetarian, err := strconv.ParseBool(r.URL.Query().Get("is_vegetarian"))
	if err != nil {
		isVegetarian = false
	}
	listMeals, err := p.MealService.ListMeals(isVegetarian, isVegan, meals, ingredients)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(listMeals); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
}
func (p MealAPI) GetMealWithIngredientsById(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	getMealWithIngredientsById, err := p.MealService.GetMealWithIngredientsById(uint(id), meals, ingredients)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(getMealWithIngredientsById); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
}
func (p MealAPI) Quality(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	garlic := r.URL.Query().Get("garlic")
	if err != nil {
		http.Error(w, err.Error(), http.StatusMethodNotAllowed)
		return
	}
	if garlic == "" {
		garlic = "high"
	}
	quality, err := p.MealService.Quality(uint(id), garlic, meals, ingredients)
	if err != nil {
		http.Error(w, err.Error(), http.StatusAlreadyReported)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(quality); err != nil {
		http.Error(w, err.Error(), http.DefaultMaxHeaderBytes)
		return
	}
}
func (p MealAPI) Price(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	garlic := r.URL.Query().Get("garlic")
	if err != nil {
		http.Error(w, err.Error(), http.StatusMethodNotAllowed)
		return
	}
	if garlic == "" {
		garlic = "high"
	}
	quality, err := p.MealService.Price(uint(id), garlic, meals, ingredients)
	if err != nil {
		http.Error(w, err.Error(), http.StatusAlreadyReported)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(quality); err != nil {
		http.Error(w, err.Error(), http.DefaultMaxHeaderBytes)
		return
	}
}
func (p MealAPI) Random(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	var budget32 float32

	budgetStr := r.URL.Query().Get("budget")
	if budgetStr != "" {
		budget, err := strconv.ParseFloat(budgetStr, 32)
		if err != nil {
			http.Error(w, err.Error(), http.StatusMethodNotAllowed)
			return
		}
		budget32 = float32(budget)
	} else {
		budget32 = 0
	}

	random, err := p.MealService.Random(budget32, meals, ingredients)
	if err != nil {
		http.Error(w, err.Error(), http.StatusAlreadyReported)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(random); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func (p MealAPI) Search(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	query := r.URL.Query().Get("query")

	random, err := p.MealService.Search(query, meals, ingredients)
	if err != nil {
		http.Error(w, err.Error(), http.StatusAlreadyReported)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(random); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func (p MealAPI) FindHighest(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	var budget32 float32

	budgetStr := r.URL.Query().Get("budget")
	if budgetStr != "" {
		budget, err := strconv.ParseFloat(budgetStr, 32)
		if err != nil {
			http.Error(w, err.Error(), http.StatusMethodNotAllowed)
			return
		}
		budget32 = float32(budget)
	} else {
		budget32 = 0
	}
	isVegan, err := strconv.ParseBool(r.URL.Query().Get("is_vegan"))
	if err != nil {
		isVegan = false
	}

	isVegetarian, err := strconv.ParseBool(r.URL.Query().Get("is_vegetarian"))
	if err != nil {
		isVegetarian = false
	}

	findHighest, err := p.MealService.FindHighest(budget32, isVegan, isVegetarian, meals, ingredients)
	if err != nil {
		http.Error(w, err.Error(), http.StatusAlreadyReported)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(findHighest); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func (p MealAPI) FindHighestOfMeal(w http.ResponseWriter, r *http.Request, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) {
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	var budget32 float32

	budgetStr := r.URL.Query().Get("budget")
	if budgetStr != "" {
		budget, err := strconv.ParseFloat(budgetStr, 32)
		if err != nil {
			http.Error(w, err.Error(), http.StatusMethodNotAllowed)
			return
		}
		budget32 = float32(budget)
	} else {
		budget32 = 0
	}
	random, err := p.MealService.FindHighestOfMeal(uint(id), budget32, meals, ingredients)
	if err != nil {
		http.Error(w, err.Error(), http.StatusAlreadyReported)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(random); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
