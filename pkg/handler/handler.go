package handler

import (
	"net/http"

	"github.com/ahmetk3436/otsimo/pkg/api"
	"github.com/ahmetk3436/otsimo/pkg/model"
)

// MealHandler handles meal-related API requests
type MealHandler struct {
	API         api.MealAPI
	Meals       map[string][]model.Meal2
	Ingredients map[string][]model.Ingredients
}

// NewMealHandler returns a new MealHandler instance
func NewMealHandler(api api.MealAPI, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) *MealHandler {
	return &MealHandler{
		API:         api,
		Meals:       meals,
		Ingredients: ingredients,
	}
}

// ListMeals handles the /listMeals endpoint
func (h *MealHandler) ListMeals(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.API.ListMeals(w, r, h.Meals, h.Ingredients)
	case "POST":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "POST method requested"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// GetMealWithIngredientsById handles the /getMeal endpoint
func (h *MealHandler) GetMealWithIngredientsById(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.API.GetMealWithIngredientsById(w, r, h.Meals, h.Ingredients)
	case "POST":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "POST method requested"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// Quality handles the /quality endpoint
func (h *MealHandler) Quality(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "GET method requested"}`))
	case "POST":
		h.API.Quality(w, r, h.Meals, h.Ingredients)
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// Price handles the /price endpoint
func (h *MealHandler) Price(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "GET method requested"}`))
	case "POST":
		h.API.Price(w, r, h.Meals, h.Ingredients)
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// Random handles the /random endpoint
func (h *MealHandler) Random(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "GET method requested"}`))
	case "POST":
		h.API.Random(w, r, h.Meals, h.Ingredients)

	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// Search handles the /search endpoint
func (h *MealHandler) Search(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.API.Search(w, r, h.Meals, h.Ingredients)
	case "POST":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "POST method requested"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// FindHighest handles the /findHighest endpoint
func (h *MealHandler) FindHighest(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "GET method requested"}`))
	case "POST":
		h.API.FindHighest(w, r, h.Meals, h.Ingredients)

		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "POST method requested"}`))
	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}

// FindHighestOfMeal handles the /findHighestOfMeal endpoint
func (h *MealHandler) FindHighestOfMeal(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{"message": "GET method requested"}`))
	case "POST":
		h.API.FindHighestOfMeal(w, r, h.Meals, h.Ingredients)

	default:
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "Can't find method requested"}`))
	}
}
