package model

type Meal struct {
	ID          int
	Name        string
	Ingredients []string
}
type Meal2 struct {
	ID          int          `json:"id"`
	Name        string       `json:"name"`
	Ingredients []Ingredient `json:"ingredients"`
}
type Meal3 struct {
	ID            int                     `json:"id"`
	Name          string                  `json:"name"`
	Price         float32                 `json:"price"`
	Quality_score int                     `json:"quality_score"`
	Ingredients   []IngredientWithQuality `json:"ingredients"`
}
type MealWithIngredients struct {
	ID      int
	Name    string
	Options []Options
}
type MealWithPriceAndIngredients struct {
	ID            int
	Price         float32
	Quality_score int
	Name          string
	Ingredients   []Ingredients
}

type Options struct {
	Name       string
	Quality    string
	Price      float32
	Per_amount string
}
type Quality struct {
	Quality int `json:"quality"`
}

type Price struct {
	Price float32 `json:"price"`
}

type Ingredient struct {
	Name         string
	Quantity     int
	QuantityType string
}
type IngredientWithQuality struct {
	Name    string
	Quality string
}
type Ingredients struct {
	Name              string
	Ingredient_groups string
	Options           []Options
}
