package parse

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/ahmetk3436/otsimo/pkg/model"
)

func ParseMeals() (map[string][]model.Meal2, error) {
	// Read the dataset.json file
	data, err := os.Open("dataset.json")
	if err != nil {
		fmt.Println("Error reading file:", err)
		return nil, err
	}
	defer data.Close()
	byteResult, _ := io.ReadAll(data)

	// Unmarshal the JSON data into a variable of type map[string][]Meal
	var meals map[string][]model.Meal2
	err = json.Unmarshal(byteResult, &meals)
	if err != nil {
		fmt.Println("Error unmarshalling JSON:", err)
		return nil, err
	}
	return meals, nil
}
func ParseIngredients() (map[string][]model.Ingredients, error) {
	// Read the dataset.json file
	data, err := os.Open("dataset.json")
	if err != nil {
		fmt.Println("Error reading file:", err)
		return nil, err
	}
	defer data.Close()
	byteResult, _ := io.ReadAll(data)

	// Unmarshal the JSON data into a variable of type map[string][]Meal
	var ingredients map[string][]model.Ingredients
	err = json.Unmarshal(byteResult, &ingredients)
	if err != nil {
		fmt.Println("Error unmarshalling JSON:", err)
		return nil, err
	}
	return ingredients, nil
}
