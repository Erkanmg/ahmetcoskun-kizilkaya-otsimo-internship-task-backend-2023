package repository

import (
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/ahmetk3436/otsimo/pkg/model"
)

// Repository defines a struct for working with posts in the database
type Repository struct {
}

const (
	highQuality   = 15
	mediumQuality = 10
	lowQuality    = 5
)

// NewRepository returns a new instance of Repository
func NewRepository() *Repository {
	return &Repository{}
}

func (r *Repository) ListMeals(isVegetarian, isVegan bool, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*[]model.Meal, error) {
	var mealList []model.Meal
	for _, mealData := range meals["meals"] {
		lastID := 0
		var ingredientGroup string
		shouldAppend := true
		for _, ingredient := range ingredients["ingredients"] {
			for _, mealIngredient := range mealData.Ingredients {
				if mealIngredient.Name == ingredient.Name {
					ingredientGroup = ingredient.Ingredient_groups
					if isVegetarian && !(strings.Contains(ingredientGroup, "vegan") || strings.Contains(ingredientGroup, "vegetarian")) {
						shouldAppend = false
						break
					} else if isVegan && !strings.Contains(ingredientGroup, "vegan") {
						shouldAppend = false
						break
					}
				}

			}
			if shouldAppend {
				var foods []string
				for _, ingredient := range mealData.Ingredients {
					foods = append(foods, ingredient.Name)
				}
				if lastID == 0 && lastID != mealData.ID {

					meal := model.Meal{
						ID:          mealData.ID,
						Name:        mealData.Name,
						Ingredients: foods,
					}
					lastID = meal.ID
					mealList = append(mealList, meal)
				}
			}
		}

	}
	return &mealList, nil
}

func (r *Repository) GetMealWithIngredientsById(id uint, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.MealWithIngredients, error) {
	var meal model.MealWithIngredients
	index := int(id - 1)
	meal.ID = meals["meals"][index].ID
	meal.Name = meals["meals"][index].Name
	for i := range ingredients {
		index, _ := strconv.Atoi(i)
		meal.Options = ingredients["ingredients"][index].Options
	}
	return &meal, nil
}

func (r *Repository) Quality(id uint, garlic string, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Quality, error) {
	meal := meals["meals"][id-1]
	var qualityScore int
	for range meal.Ingredients {
		qualityScore = qualityCalculator(garlic, qualityScore)
	}

	var quality model.Quality
	quality.Quality = qualityScore

	return &quality, nil
}

func (r *Repository) Price(id uint, garlic string, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Price, error) {
	meal := meals["meals"][id-1]
	var price float32
	for i := range meal.Ingredients {
		ingredientName := meal.Ingredients[i].Name
		ingredientList, ok := ingredients["ingredients"]
		if !ok {
			return nil, fmt.Errorf("ingredients list not found")
		}
		var optionPrice float32
		for j := range ingredientList {
			if ingredientList[j].Name == ingredientName {
				switch garlic {
				case "high":
					optionPrice = ingredientList[j].Options[0].Price
				case "medium":
					optionPrice = ingredientList[j].Options[1].Price
				case "low":
					optionPrice = ingredientList[j].Options[2].Price
				}
				break
			}
			var addOn float32
			if garlic != "low" {
				addOn = 0.05
			} else {
				addOn = 0.10
			}
			optionPrice = ((float32(meal.Ingredients[i].Quantity) / 1000.0) * optionPrice) + addOn
		}
		price += optionPrice
	}

	totalPrice := &model.Price{Price: price}

	return totalPrice, nil
}

func (r *Repository) Random(budget float32, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal3, error) {
	var meal model.Meal3
	var ingredientsWithQuality []model.IngredientWithQuality
	var price float32
	var quality_score int
	for {
		price = 0
		quality_score = 0
		ingredientsWithQuality = []model.IngredientWithQuality{}
		selectedMeal := meals["meals"][rand.Intn(len(meals["meals"]))]
		for i := range selectedMeal.Ingredients {
			ingredient := ingredients["ingredients"][i]
			ingredientOption := ingredient.Options[rand.Intn(len(ingredient.Options))]
			price += priceCalculator(ingredientOption, price, selectedMeal, i)
			ingredientsWithQuality = append(ingredientsWithQuality, model.IngredientWithQuality{
				Name:    ingredientOption.Name,
				Quality: ingredientOption.Quality,
			})
			quality_score = qualityCalculator(ingredientOption.Quality, quality_score)
		}
		if budget == 0 || price <= budget {
			meal = model.Meal3{
				ID:            selectedMeal.ID,
				Name:          selectedMeal.Name,
				Price:         price,
				Quality_score: quality_score,
				Ingredients:   ingredientsWithQuality,
			}
			return &meal, nil
		}
	}

}

func (r *Repository) Search(query string, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal, error) {
	var meal model.Meal
	for _, m := range meals["meals"] {
		if strings.Contains(strings.ToLower(m.Name), strings.ToLower(query)) {
			meal.ID = m.ID
			meal.Name = m.Name
			var foods []string
			for _, i := range m.Ingredients {
				foods = append(foods, i.Name)
			}
			meal.Ingredients = foods
			break
		} else {
			continue
		}
	}
	return &meal, nil
}
func (r *Repository) FindHighest(budget float32, is_vegan, is_vegetarian bool, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal3, error) {
	var ingredientsWithQuality []model.IngredientWithQuality
	var price float32
	var quality_score int

	bestMeal := model.Meal3{}
	bestQuality := 0
	bestPrice := float32(math.MaxInt32)

	rand.Seed(time.Now().UnixNano())
	randomIndex := rand.Intn(len(meals["meals"]))
	selectedMeal := meals["meals"][randomIndex]

	price = 0
	quality_score = 0
	ingredientsWithQuality = []model.IngredientWithQuality{}

	for i := range selectedMeal.Ingredients {
		ingredient := ingredients["ingredients"][i]
		for j := range ingredient.Options {
			ingredientOption := ingredient.Options[j]
			ingredientGroups := ingredient.Ingredient_groups
			shouldAppend := true

			if is_vegetarian && !(strings.Contains(ingredientGroups, "vegan") || strings.Contains(ingredientGroups, "vegetarian")) {
				shouldAppend = false
			} else if is_vegan && !strings.Contains(ingredientGroups, "vegan") {
				shouldAppend = false
			}

			if shouldAppend {
				price += ingredientOption.Price
				ingredientsWithQuality = append(ingredientsWithQuality, model.IngredientWithQuality{
					Name:    ingredientOption.Name,
					Quality: ingredientOption.Quality,
				})
				quality_score += qualityCalculator(ingredientOption.Quality, quality_score)
				break
			}
		}
	}

	if budget >= price && price <= bestPrice && quality_score >= bestQuality {
		bestPrice = price
		bestQuality = quality_score
		bestMeal.Name = selectedMeal.Name
		bestMeal.ID = selectedMeal.ID
	}

	meal := model.Meal3{
		ID:            bestMeal.ID,
		Name:          bestMeal.Name,
		Price:         bestPrice,
		Quality_score: bestQuality,
		Ingredients:   ingredientsWithQuality,
	}

	return &meal, nil

}
func (r *Repository) FindHighestOfMeal(id uint, budget float32, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal3, error) {
	var bestMeal *model.Meal3
	bestQuality := 0
	bestPrice := float32(0)
	for _, meal := range meals["meals"] {
		totalPrice := float32(0)
		var mealIngredients []model.IngredientWithQuality
		for _, ingredient := range meal.Ingredients {
			for _, ingredientInfo := range ingredients["ingredients"] {
				if ingredient.Name == ingredientInfo.Name {
					for _, option := range ingredientInfo.Options {
						totalPrice += option.Price
						mealIngredients = append(mealIngredients, model.IngredientWithQuality{Name: option.Name, Quality: option.Quality})
						break
					}
				}
			}
		}
		if budget > 0 && totalPrice > budget {
			continue
		}
		var qualityScore int
		for _, ingredient := range mealIngredients {
			if ingredient.Quality == "High" {
				qualityScore++
			}
		}
		if qualityScore > bestQuality || (qualityScore == bestQuality && totalPrice < bestPrice) {
			bestQuality = qualityScore
			bestPrice = totalPrice
			bestMeal = &model.Meal3{ID: meal.ID, Name: meal.Name, Price: totalPrice, Quality_score: qualityScore, Ingredients: mealIngredients}
		}
	}
	return bestMeal, nil
}

func qualityCalculator(quality string, quality_score int) int {
	switch quality {
	case "high":
		quality_score += highQuality
	case "medium":
		quality_score += mediumQuality
	case "low":
		quality_score += lowQuality
	}
	return quality_score
}

func priceCalculator(ingredientOption model.Options, price float32, selectedMeal model.Meal2, i int) float32 {
	var addOn float32
	if ingredientOption.Quality != "low" {
		addOn = 0.05
	} else {
		addOn = 0.10
	}
	price += ((float32(selectedMeal.Ingredients[i].Quantity) / 1000.0) * ingredientOption.Price) + addOn
	return price
}
