package router

import (
	"net/http"

	"github.com/ahmetk3436/otsimo/pkg/handler"
)

// NewRouter returns a new router instance
func NewRouter(mealHandler handler.MealHandler) http.Handler {
	r := http.NewServeMux()
	r.HandleFunc("/listMeals", mealHandler.ListMeals)
	r.HandleFunc("/getMeal", mealHandler.GetMealWithIngredientsById)
	r.HandleFunc("/quality", mealHandler.Quality)
	r.HandleFunc("/price", mealHandler.Price)
	r.HandleFunc("/random", mealHandler.Random)
	r.HandleFunc("/search", mealHandler.Search)
	r.HandleFunc("/findHighest", mealHandler.FindHighest)
	r.HandleFunc("/findHighestOfMeal", mealHandler.FindHighestOfMeal)
	return r
}
