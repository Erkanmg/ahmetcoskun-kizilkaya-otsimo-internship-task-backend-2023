package service

import (
	"github.com/ahmetk3436/otsimo/pkg/model"
	"github.com/ahmetk3436/otsimo/pkg/repository"
)

// MealService ...
type MealService struct {
	MealRepository *repository.Repository
}

// NewMealService ...
func NewMealService(p *repository.Repository) MealService {
	return MealService{MealRepository: p}
}

func (p *MealService) ListMeals(is_vegetarian, is_vegan bool, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*[]model.Meal, error) {
	return p.MealRepository.ListMeals(is_vegetarian, is_vegan, meals, ingredients)
}
func (p *MealService) GetMealWithIngredientsById(id uint, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.MealWithIngredients, error) {
	return p.MealRepository.GetMealWithIngredientsById(id, meals, ingredients)
}
func (p *MealService) Quality(id uint, garlic string, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Quality, error) {
	return p.MealRepository.Quality(id, garlic, meals, ingredients)
}
func (p *MealService) Price(id uint, garlic string, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Price, error) {
	return p.MealRepository.Price(id, garlic, meals, ingredients)
}
func (p *MealService) Random(budget float32, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal3, error) {
	return p.MealRepository.Random(budget, meals, ingredients)
}
func (p *MealService) Search(query string, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal, error) {
	return p.MealRepository.Search(query, meals, ingredients)
}
func (p *MealService) FindHighest(budget float32, is_vegan, is_vegetarian bool, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal3, error) {
	return p.MealRepository.FindHighest(budget, is_vegan, is_vegetarian, meals, ingredients)
}
func (p *MealService) FindHighestOfMeal(id uint, budget float32, meals map[string][]model.Meal2, ingredients map[string][]model.Ingredients) (*model.Meal3, error) {
	return p.MealRepository.FindHighestOfMeal(id, budget, meals, ingredients)
}
